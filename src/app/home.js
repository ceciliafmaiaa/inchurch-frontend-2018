angular
  .module('app')
  .component('app', {
    templateUrl: 'app/home.html',
    controllerAs: 'vm',
    controller: function (APIService, $timeout, $state) {
      var vm = this;

      vm.users = [];
      vm.success = false;
      vm.successMsg = '';
      vm.currentUserId = '';
      vm.userData = {};

      vm.setCurrentUser = function(userId, userData) {
        vm.currentUserId = userId;
        vm.userData = {};

        if (userData) {
          vm.userData = {
            'first_name': userData.first_name,
            'last_name': userData.last_name,
            'avatar': userData.avatar
          };
        }
      };

      vm.deleteUser = function() {
        var userId = vm.currentUserId;
        var modalId = '#modalDelete';
        var successMsg = 'User successfully removed!';
        
        APIService.deleteUser(userId).then(function(resp) { 
          modalSuccess(successMsg, modalId);
        });

      };

      vm.updateUser = function() {
        var userId = vm.currentUserId;
        var userData = vm.userData;
        var modalId = '#modalEdit';
        var successMsg = 'User successfully updated!';

        APIService.updateUser(userId, userData).then(function(resp) {
          modalSuccess(successMsg, modalId);
        });

      };

      vm.createUser = function(userData) {
        var modalId = '#modalCreate';
        var successMsg = 'User successfully created!';

        APIService.createUser(userData).then(function(resp) {
          modalSuccess(successMsg, modalId);
        });

      };

      vm.goToUserDetails = function(userId) {
        console.log(userId);
        $state.go('userDetail', {
          userId: userId
        });
      };

      getUserList();

      function getUserList() {
        APIService.getUserList().then(function(resp) {
          var data = resp.data.data;
          vm.users = data;
        });
      }


      function modalSuccess(successMsg, modalId) {
        vm.success = true;
        vm.successMsg = successMsg;

        $timeout(function() {
          vm.success = false;
          vm.successMsg = '';
          vm.userData = {};
          $(modalId).modal('hide');
        }, 1500);
      }


    }
  });
