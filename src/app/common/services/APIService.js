function APIService($http) {

  var url = 'https://reqres.in/api/';

  var service = {};

  service.getUserList = function() {
    return $http.get(url + 'users?per_page=10');
  };

  service.getUserDetails = function(userId) {
    return $http.get(url + 'users/' + userId);
  };

  service.createUser = function(userData) {
    return $http.post(url + 'users/', userData);
  };

  service.deleteUser = function(userId) {
    return $http.delete(url + 'users/' + userId)
  };

  service.updateUser = function(userId, userData) {
    return $http.put(url + 'users/' + userId, userData);
  };

  return service;
}

APIService.$inject = ['$http'];

angular
  .module('app')
  .service('APIService', APIService);

