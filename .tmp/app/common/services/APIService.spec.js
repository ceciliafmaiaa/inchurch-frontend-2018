describe('APIService service', function () {
  beforeEach(module('app'));
  it('should', angular.mock.inject(function (APIService) {
    expect(APIService.getData()).toEqual(3);
  }));
});
