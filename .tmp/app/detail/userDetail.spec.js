describe('userDetail component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('userDetail', function () {
      return {
        templateUrl: 'app/userDetail.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<userDetail></userDetail>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
