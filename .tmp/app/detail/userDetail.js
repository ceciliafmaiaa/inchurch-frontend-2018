angular
  .module('app')
  .component('userDetail', {
    templateUrl: 'app/detail/userDetail.html',
    controllerAs: 'vm',
    controller: function($stateParams, APIService) {
      var vm = this;
      var userId = $stateParams.userId;

      vm.userData = {};

      getUserDetails(userId);

      function getUserDetails(userId) {
        APIService.getUserDetails(userId).then(function(resp) {
          var data = resp.data.data;
          vm.userData = data;
        });
      }

    }
  });

